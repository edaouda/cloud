```bash
# 1. Une première VM
ssh azureuser@213.199.128.93

# 2. Azure CLI
curl -L https://aka.ms/InstallAzureCli | bash
exec -l $SHELL
az --help
az login # valider grâce au lien donné vers le navigateur
az group list
az vm list
az --help

az interactive
az vm --help
Subgroups:
    disk                   : Manage the managed data disks attached to a VM.
    image                  : Information on available virtual machine images. # az vm image list
    nic                    : Manage network interfaces. See also `az network nic`. # vm nic list --vm-name  b3-cloud-2021-tp4-1-vm --resource-group b3-cloud-2021-tp4-1-resource-group
    user                   : Manage user accounts for a VM.

Commands:
    capture                : Capture information for a stopped VM.
    create                 : Create an Azure Virtual Machine.
    delete                 : Delete a VM.
    list                   : List details of Virtual Machines.
    list-ip-addresses      : List IP addresses associated with a VM. # vm list-ip-addresses --name b3-cloud-2021-tp4-1-vm --resource-group b3-cloud-2021-tp4-1-resource-group -o table
    open-port              : Opens a VM to inbound traffic on specified ports. # vm  open-port --port 80 --name b3-cloud-2021-tp4-1-vm --resource-group b3-cloud-2021-tp4-1-resource-group
    redeploy               : Redeploy an existing VM.
    restart                : Restart VMs.
    show                   : Get the details of a VM. # vm show --name b3-cloud-2021-tp4-1-vm --resource-group b3-cloud-2021-tp4-1-resource-group  -o table
    start                  : Start a stopped VM.
    stop                   : Power off (stop) a running VM.
# Plus d'info sur les commandes: https://aka.ms/cli_ref
```