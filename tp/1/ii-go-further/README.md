```bash
vagrant ssh ubuntu-cloud-init
python3 --version
	#Python 3.8.10

ls /home/
	#ansible-admin  docker-admin  vagrant

# docker-admin
sudo su docker-admin
getent passwd | grep docker-admin
	#grep ansible-admindocker-admin:x:1001:1002::/home/docker-admin:/bin/bash
	#docker-admin docker
cat ~/.ssh/authorized_keys
	#ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC35xnuCeDhQvh47jquJ4CTUg0NxG4tOsSzM+tGCSwIMJPBwz0Jw5PbdINGiDef+eFLD41B1dqcQi/OwYCDJJUlENxNKBj3EMlA2RL1UCWibEJaVoDUD2NrYiwwtKcPscuthImZwD2kAm+W7ITCaSbeyS5A4w/pVFyDAlLJsJtiLVWSvX+fKUWOYZgQgPhYgfYuTRebEU77Khd01mRFu76PhHiV/cfCecIO7O2R0431RIhtaDzWOuYwQKd/ORbWYbHmav9wqOvBs4IwBJYusRPJKoQ0+Gdq6XF9p1uB2Yf36Zh/xfyDxl7I8Qb2kQGseTP2J/Dd6OOBIN7b2Uq8uS5/okb5Qm9iqTKvWFVlSI83W9yQH25x4pkHqu3hxygfg45low/t3EvX6dRTodnnehAukFL8pQBsTeRoh4oxWIVxKWkpnGhRNdBiAfVvhYkCjsHlRiw8XyT/91MnnUSrzEmL4kixlBE++rBJyBnt9SK2oF9UDaSWrkqTzqLKzMOYrhJvbOGGYN7DQdo/Q8WQvYjWT2UYQJcYmOE0ow8Vomrw+EP66DGnO0ZjcNmGKz0VbQRiVWcUW+Or38mW8ye7nmuyF2wjP48pZ7JTDJNqitM3aVcbz3ZzqwcodiiU95pDVRhA7t69YqeIt6OchlqDz5qBLuyiR4zaWYHXfSGmeSIMgw== docker-admin@nowhere.docker-admin
exit

# ansible-admin
sudo su ansible-admin
getent passwd | grep ansible-admin
	#ansible-admin:x:1002:1003::/home/ansible-admin:/bin/bash
cat ~/.ssh/authorized_keys
	#ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC35xnuCeDhQvh47jquJ4CTUg0NxG4tOsSzM+tGCSwIMJPBwz0Jw5PbdINGiDef+eFLD41B1dqcQi/OwYCDJJUlENxNKBj3EMlA2RL1UCWibEJaVoDUD2NrYiwwtKcPscuthImZwD2kAm+W7ITCaSbeyS5A4w/pVFyDAlLJsJtiLVWSvX+fKUWOYZgQgPhYgfYuTRebEU77Khd01mRFu76PhHiV/cfCecIO7O2R0431RIhtaDzWOuYwQKd/ORbWYbHmav9wqOvBs4IwBJYusRPJKoQ0+Gdq6XF9p1uB2Yf36Zh/xfyDxl7I8Qb2kQGseTP2J/Dd6OOBIN7b2Uq8uS5/okb5Qm9iqTKvWFVlSI83W9yQH25x4pkHqu3hxygfg45low/t3EvX6dRTodnnehAukFL8pQBsTeRoh4oxWIVxKWkpnGhRNdBiAfVvhYkCjsHlRiw8XyT/91MnnUSrzEmL4kixlBE++rBJyBnt9SK2oF9UDaSWrkqTzqLKzMOYrhJvbOGGYN7DQdo/Q8WQvYjWT2UYQJcYmOE0ow8Vomrw+EP66DGnO0ZjcNmGKz0VbQRiVWcUW+Or38mW8ye7nmuyF2wjP48pZ7JTDJNqitM3aVcbz3ZzqwcodiiU95pDVRhA7t69YqeIt6OchlqDz5qBLuyiR4zaWYHXfSGmeSIMgw== docker-admin@nowhere.docker-admin
sudo -l
	<<EOF
Matching Defaults entries for ansible-admin on ubuntu:
    env_reset, mail_badpass, secure_path=/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/snap/bin

User ansible-admin may run the following commands on ubuntu:
    (ALL) NOPASSWD: ALL
EOF
exit

docker --version
	#Docker version 20.10.12, build e91ed57
```
