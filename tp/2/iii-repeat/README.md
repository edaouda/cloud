```bash
# III. Repeat
## 1. NGINX
cat << EOF > inventories/vagrant_lab/host_vars/node1.tp2.cloud.yml
vhosts:
  - test2:
    nginx_servername: test2
    nginx_port: 8082
    nginx_webroot: /var/www/html/test2
    nginx_index_content: "<h1>teeeeeest 2</h1>"
  - test3:
    nginx_servername: test3
    nginx_port: 8083
    nginx_webroot: /var/www/html/test3
    nginx_index_content: "<h1>teeeeeest 3</h1>"
EOF
cat << EOF > inventories/vagrant_lab/host_vars/node2.tp2.cloud.yml
vhosts_to_remove:
  - test2:
    nginx_servername: test2
    nginx_port: 8082
    nginx_webroot: /var/www/html/test2
    nginx_index_content: "<h1>teeeeeest 2</h1>"
  - test3:
    nginx_servername: test3
    nginx_port: 8083
    nginx_webroot: /var/www/html/test3
    nginx_index_content: "<h1>teeeeeest 3</h1>"
EOF
cat << EOF > roles/nginx/tasks/main.yml
- name: Install NGINX
  import_tasks: install.yml
  notify: "Restart nginx"

- name: Configure NGINX
  import_tasks: config.yml
  notify: "Restart nginx"

- name: Deploy VirtualHosts
  import_tasks: add_vhosts.yml
  when: vhosts is defined
  notify: "Restart nginx"

- name: Delete VirtualHosts
  import_tasks: remove_vhosts.yml
  when: vhosts_to_remove is defined
  notify: "Restart nginx"
EOF
cat << EOF > roles/nginx/tasks/add_vhosts.yml
- name: Create webroot
  file:
    path: "{{ item.nginx_webroot }}"
    state: directory
  with_items: "{{ vhosts }}"

- name: Create index
  copy:
    dest: "{{ item.nginx_webroot }}/index.html"
    content: "{{ item.nginx_index_content }}"
    mode: '0644'
  with_items: "{{ vhosts }}"

- name: NGINX Virtual Host
  template:
    src: vhost.conf.j2
    dest: /etc/nginx/conf.d/{{ item.nginx_servername }}.conf
    mode: '0644'
  with_items: "{{ vhosts }}"

- name: Allow all access to tcp port
  ufw:
    rule: allow
    port: "{{ item.nginx_port }}"
  with_items: "{{ vhosts }}"
EOF
cat << EOF > roles/nginx/templates/vhost.conf.j2
server {
        listen {{ item.nginx_port }} ;
        server_name {{ item.nginx_servername }};

        location / {
            root {{ item.nginx_webroot }};
            index index.html;
        }
}
EOF
mkdir roles/nginx/handlers
cat << EOF > roles/nginx/handlers/main.yml
---
- name: Restart nginx
  service:
    name: nginx
    state: restarted
EOF
ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml
curl node1.tp2.cloud:8082
curl node1.tp2.cloud:8083

## 2. Common
cat << EOF > inventories/vagrant_lab/group_vars/ynov.yml
users:
  - le_nain:
    username: le_nain
    password: le_nain
    homedir: /home/le_nain
    access: 
    groups: admin,sudo
    ssh_public_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzMYPGClWlII1ZlRZUBf43ECExsr3Wd2fJzWtEgsCQZO+ztrmu7uAP+ovEndTDPJe1q3m9XntDSEI7jjQNM6h4enY1M7f7v1r/C7UHgLhqAP3PaEVcCq3yFcscnB4jOLLG8SonqVJO16L84J+Jwb2LwfsMyc0tUmtBqh4rbePzwrj9+ukExsRiR/7R4NqvBEYV/CFFej/cRRZeoaJJmWcGULJHXMvSK0jwFQOgpfXL6DpXQlLRwdl5xW465ZMXuOm4BixjWDkDO+8HVhOD4DxitixBmjUaGdccMfKH33Z031fdSYMqw8kBAfwoyU4CJzQWo39Maf1cThAuUFqza6/TMn7aWlpvcgX1DKVV6Qit788t7bf5DavotLb0ptA3K8W0dsR3TBg/Ixxtbt02I+RR7h7ghCPpbnNmJbblsGyvzJiDvw56srUoE8D67XGuBHC+bh8YZ/sFE9BUNTr8brrdsx8u6ZG+VEg/FoPREkhoP7U6r4DDleSGzfu8wI4ocL8= vagrant@ubuntu
  - l_elfe:
    username: l_elfe
    password: l_elfe
    homedir: /home/l_elfe
    access: 
    groups: admin,sudo
    ssh_public_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzMYPGClWlII1ZlRZUBf43ECExsr3Wd2fJzWtEgsCQZO+ztrmu7uAP+ovEndTDPJe1q3m9XntDSEI7jjQNM6h4enY1M7f7v1r/C7UHgLhqAP3PaEVcCq3yFcscnB4jOLLG8SonqVJO16L84J+Jwb2LwfsMyc0tUmtBqh4rbePzwrj9+ukExsRiR/7R4NqvBEYV/CFFej/cRRZeoaJJmWcGULJHXMvSK0jwFQOgpfXL6DpXQlLRwdl5xW465ZMXuOm4BixjWDkDO+8HVhOD4DxitixBmjUaGdccMfKH33Z031fdSYMqw8kBAfwoyU4CJzQWo39Maf1cThAuUFqza6/TMn7aWlpvcgX1DKVV6Qit788t7bf5DavotLb0ptA3K8W0dsR3TBg/Ixxtbt02I+RR7h7ghCPpbnNmJbblsGyvzJiDvw56srUoE8D67XGuBHC+bh8YZ/sFE9BUNTr8brrdsx8u6ZG+VEg/FoPREkhoP7U6r4DDleSGzfu8wI4ocL8= vagrant@ubuntu
  - le_ranger:
    username: le_ranger
    password: le_ranger
    homedir: /home/le_ranger
    access: 
    groups: admin,sudo
    ssh_public_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzMYPGClWlII1ZlRZUBf43ECExsr3Wd2fJzWtEgsCQZO+ztrmu7uAP+ovEndTDPJe1q3m9XntDSEI7jjQNM6h4enY1M7f7v1r/C7UHgLhqAP3PaEVcCq3yFcscnB4jOLLG8SonqVJO16L84J+Jwb2LwfsMyc0tUmtBqh4rbePzwrj9+ukExsRiR/7R4NqvBEYV/CFFej/cRRZeoaJJmWcGULJHXMvSK0jwFQOgpfXL6DpXQlLRwdl5xW465ZMXuOm4BixjWDkDO+8HVhOD4DxitixBmjUaGdccMfKH33Z031fdSYMqw8kBAfwoyU4CJzQWo39Maf1cThAuUFqza6/TMn7aWlpvcgX1DKVV6Qit788t7bf5DavotLb0ptA3K8W0dsR3TBg/Ixxtbt02I+RR7h7ghCPpbnNmJbblsGyvzJiDvw56srUoE8D67XGuBHC+bh8YZ/sFE9BUNTr8brrdsx8u6ZG+VEg/FoPREkhoP7U6r4DDleSGzfu8wI4ocL8= vagrant@ubuntu
EOF
#test: sur node1.tp2.cloud, su - le_nain avec password le_nain -> su: Authentication failure, pourquoi: password n'est pas le même ?

cat << EOF > roles/common/tasks/users.yml
- name: Add common users
  ansible.builtin.user:
    name: "{{ item.username }}"
    home: "{{ item.homedir }}"
    groups: "{{ item.groups }}"
  with_items: "{{ users }}"

- name: Set authorized key taken
  ansible.posix.authorized_key:
    user: "{{ item.username }}"
    state: present
    key: "{{ item.ssh_public_key }}"
  with_items: "{{ users }}"
EOF
# 3. Dynamic loadbalancer
mkdir roles/webapp
mkdir roles/webapp/tasks
cat << EOF > roles/webapp/tasks/install.yml
- name: Install NGINX
  apt:
    name: nginx
    state: present

- name: Allow all access to tcp port 80
  ufw:
    rule: allow
    port: "80"
EOF
cat << EOF > roles/webapp/tasks/main.yml
- name: Install NGINX
  import_tasks: install.yml
  notify: "Restart nginx"
EOF
mkdir roles/webapp/handlers
cat << EOF > roles/webapp/handlers/main.yml
---
- name: Restart nginx
  service:
    name: nginx
    state: restarted
EOF
cat << EOF > playbooks/main.yml
- hosts: ynov
  become: true #besoin pour executer le playbook en sudo
  roles:
    - common
    - nginx
    - webapp
EOF
ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml
curl node1.tp2.cloud
curl node2.tp2.cloud

# Add new machine
cat << EOF >> inventories/vagrant_lab/hosts.ini
node3.tp2.cloud
EOF

mkdir roles/rproxy
mkdir roles/rproxy/templates
cat << EOF > roles/rproxy/templates/nginx.conf.j2
user www-data;
worker_processes auto;
pid /run/nginx.pid;
include /etc/nginx/modules-enabled/*.conf;

events {
        worker_connections 768;
        # multi_accept on;
}

http {
{% for item in hosts_deploy_webapp %}
 server {
    listen {{ item.host_port }};

    location / {
      proxy_pass http://{{ item.host_ip }}/;
    }
 }
{% endfor %}

        ##
        # Basic Settings
        ##

        sendfile on;
        tcp_nopush on;
        tcp_nodelay on;
        keepalive_timeout 65;
        types_hash_max_size 2048;
        # server_tokens off;

        # server_names_hash_bucket_size 64;
        # server_name_in_redirect off;

        include /etc/nginx/mime.types;
        default_type application/octet-stream;

        ##
        # SSL Settings
        ##

        ssl_protocols TLSv1 TLSv1.1 TLSv1.2 TLSv1.3; # Dropping SSLv3, ref: POODLE
        ssl_prefer_server_ciphers on;

        ##
        # Logging Settings
        ##

        access_log /var/log/nginx/access.log;
        error_log /var/log/nginx/error.log;

        ##
        # Gzip Settings
        ##

        gzip on;

        # gzip_vary on;
        # gzip_proxied any;
        # gzip_comp_level 6;
        # gzip_buffers 16 8k;
        # gzip_http_version 1.1;
        # gzip_types text/plain text/css application/json application/javascript text/xml application/xml application/xml+rss text/javascript;

        ##
        # Virtual Host Configs
        ##

        include /etc/nginx/conf.d/*.conf;
        include /etc/nginx/sites-enabled/*;


}
EOF

mkdir roles/rproxy/tasks
cat << EOF > roles/rproxy/tasks/config.yml
- name: Configure rproxy
  template:
    src: nginx.conf.j2
    dest: /etc/nginx/nginx.conf
    mode: '0644'
EOF
cat << EOF > roles/rproxy/tasks/main.yml
- name: Configure rproxy
  import_tasks: config.yml
  notify: "Restart nginx"
EOF
cat << EOF > inventories/vagrant_lab/host_vars/node3.tp2.cloud.yml
is_rproxy: true
EOF
cat << EOF >> inventories/vagrant_lab/group_vars/ynov.yml
hosts_deploy_webapp:
  - node1.tp2.cloud:
    host_hostname: node1.tp2.cloud
    host_ip: 192.168.1.3
    host_port: 8083
EOF
mkdir roles/rproxy/handlers
cat << EOF > roles/rproxy/handlers/main.yml
---
- name: Restart nginx
  service:
    name: nginx
    state: restarted
EOF
cat << EOF > playbooks/main.yml
- hosts: ynov
  become: true #besoin pour executer le playbook en sudo
  roles:
    - common
    - nginx
    - webapp
    - { role: rproxy, when: "is_rproxy is defined and is_rproxy == true" }
EOF
ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml
curl node3.tp2.cloud:8083
curl node3.tp2.cloud:8084
curl node3.tp2.cloud:8084
  #curl: (7) Failed to connect to node3.tp2.cloud port 8084: Connection refused

# màj
rm roles/rproxy/templates/nginx.conf.j2
cat << EOF > roles/rproxy/templates/webapp.conf.j2
upstream ip_hosts_add_webapp {

{% for item in ip_hosts_add_webapp %}
  server {{ item }};
{% endfor %}

}

server {
    server_name webapp.com;

    location / {
        proxy_pass http://ip_hosts_add_webapp;
        proxy_set_header    Host \$host;
    }
}
EOF
cat << EOF > roles/rproxy/tasks/config.yml
- name: Configure rproxy
  template:
    src: webapp.conf.j2
    dest: /etc/nginx/conf.d/webapp.conf
    mode: '0644'
EOF
cat << EOF > playbooks/main.yml
- hosts: ynov
  become: true #besoin pour executer le playbook en sudo
  roles:
    - common
    - nginx
    - webapp
    - { role: rproxy, when: "is_host_rproxy is defined and is_host_rproxy == true" }
EOF
cat << EOF > inventories/vagrant_lab/host_vars/node3.tp2.cloud.yml
is_host_rproxy: true
EOF

# modifier la ligne "127.0.0.1 localhost webapp.com" de /etc/hosts de node3.tp2.cloud

cat << EOF > 

EOF
```