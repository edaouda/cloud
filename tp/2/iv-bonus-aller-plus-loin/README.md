```bash
# IV. Bonus : Aller plus loin
# 1. Vault Ansible
cat << EOF > inventories/vagrant_lab/group_vars/ynov.yml
users:
  - le_nain:
    username: le_nain
    password: le_nain
    homedir: /home/le_nain
    access: 
    groups: admin,sudo
  - l_elfe:
    username: l_elfe
    password: l_elfe
    homedir: /home/l_elfe
    access: 
    groups: admin,sudo
  - le_ranger:
    username: le_ranger
    password: le_ranger
    homedir: /home/le_ranger
    access: 
    groups: admin,sudo
hosts_deploy_webapp:
  - node1.tp2.cloud:
    host_hostname: node1.tp2.cloud
    host_ip: 192.168.1.3
  - node2.tp2.cloud:
    host_hostname: node2.tp2.cloud
    host_ip: 192.168.1.4
hosts_deploy_webapp:
  - node1.tp2.cloud:
    host_hostname: node1.tp2.cloud
    host_ip: 192.168.1.3
    host_port: 8083
  - node2.tp2.cloud:
    host_hostname: node2.tp2.cloud
    host_ip: 192.168.1.4
    host_port: 8084
hosts_deploy_webapp:
  - node1.tp2.cloud:
    host_hostname: node1.tp2.cloud
    host_ip: 192.168.1.3
    host_port: 8083
EOF
cat << EOF > users_password.yml
users:
  - le_nain:
    ssh_public_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzMYPGClWlII1ZlRZUBf43ECExsr3Wd2fJzWtEgsCQZO+ztrmu7uAP+ovEndTDPJe1q3m9XntDSEI7jjQNM6h4enY1M7f7v1r/C7UHgLhqAP3PaEVcCq3yFcscnB4jOLLG8SonqVJO16L84J+Jwb2LwfsMyc0tUmtBqh4rbePzwrj9+ukExsRiR/7R4NqvBEYV/CFFej/cRRZeoaJJmWcGULJHXMvSK0jwFQOgpfXL6DpXQlLRwdl5xW465ZMXuOm4BixjWDkDO+8HVhOD4DxitixBmjUaGdccMfKH33Z031fdSYMqw8kBAfwoyU4CJzQWo39Maf1cThAuUFqza6/TMn7aWlpvcgX1DKVV6Qit788t7bf5DavotLb0ptA3K8W0dsR3TBg/Ixxtbt02I+RR7h7ghCPpbnNmJbblsGyvzJiDvw56srUoE8D67XGuBHC+bh8YZ/sFE9BUNTr8brrdsx8u6ZG+VEg/FoPREkhoP7U6r4DDleSGzfu8wI4ocL8= vagrant@ubuntu
  - l_elfe:
    ssh_public_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzMYPGClWlII1ZlRZUBf43ECExsr3Wd2fJzWtEgsCQZO+ztrmu7uAP+ovEndTDPJe1q3m9XntDSEI7jjQNM6h4enY1M7f7v1r/C7UHgLhqAP3PaEVcCq3yFcscnB4jOLLG8SonqVJO16L84J+Jwb2LwfsMyc0tUmtBqh4rbePzwrj9+ukExsRiR/7R4NqvBEYV/CFFej/cRRZeoaJJmWcGULJHXMvSK0jwFQOgpfXL6DpXQlLRwdl5xW465ZMXuOm4BixjWDkDO+8HVhOD4DxitixBmjUaGdccMfKH33Z031fdSYMqw8kBAfwoyU4CJzQWo39Maf1cThAuUFqza6/TMn7aWlpvcgX1DKVV6Qit788t7bf5DavotLb0ptA3K8W0dsR3TBg/Ixxtbt02I+RR7h7ghCPpbnNmJbblsGyvzJiDvw56srUoE8D67XGuBHC+bh8YZ/sFE9BUNTr8brrdsx8u6ZG+VEg/FoPREkhoP7U6r4DDleSGzfu8wI4ocL8= vagrant@ubuntu
  - le_ranger:
    ssh_public_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzMYPGClWlII1ZlRZUBf43ECExsr3Wd2fJzWtEgsCQZO+ztrmu7uAP+ovEndTDPJe1q3m9XntDSEI7jjQNM6h4enY1M7f7v1r/C7UHgLhqAP3PaEVcCq3yFcscnB4jOLLG8SonqVJO16L84J+Jwb2LwfsMyc0tUmtBqh4rbePzwrj9+ukExsRiR/7R4NqvBEYV/CFFej/cRRZeoaJJmWcGULJHXMvSK0jwFQOgpfXL6DpXQlLRwdl5xW465ZMXuOm4BixjWDkDO+8HVhOD4DxitixBmjUaGdccMfKH33Z031fdSYMqw8kBAfwoyU4CJzQWo39Maf1cThAuUFqza6/TMn7aWlpvcgX1DKVV6Qit788t7bf5DavotLb0ptA3K8W0dsR3TBg/Ixxtbt02I+RR7h7ghCPpbnNmJbblsGyvzJiDvw56srUoE8D67XGuBHC+bh8YZ/sFE9BUNTr8brrdsx8u6ZG+VEg/FoPREkhoP7U6r4DDleSGzfu8wI4ocL8= vagrant@ubuntu
EOF
cat << EOF > users_password.yml
users:
  - le_nain:
    username: le_nain
    password: le_nain
    homedir: /home/le_nain
    access: 
    groups: admin,sudo
    ssh_public_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzMYPGClWlII1ZlRZUBf43ECExsr3Wd2fJzWtEgsCQZO+ztrmu7uAP+ovEndTDPJe1q3m9XntDSEI7jjQNM6h4enY1M7f7v1r/C7UHgLhqAP3PaEVcCq3yFcscnB4jOLLG8SonqVJO16L84J+Jwb2LwfsMyc0tUmtBqh4rbePzwrj9+ukExsRiR/7R4NqvBEYV/CFFej/cRRZeoaJJmWcGULJHXMvSK0jwFQOgpfXL6DpXQlLRwdl5xW465ZMXuOm4BixjWDkDO+8HVhOD4DxitixBmjUaGdccMfKH33Z031fdSYMqw8kBAfwoyU4CJzQWo39Maf1cThAuUFqza6/TMn7aWlpvcgX1DKVV6Qit788t7bf5DavotLb0ptA3K8W0dsR3TBg/Ixxtbt02I+RR7h7ghCPpbnNmJbblsGyvzJiDvw56srUoE8D67XGuBHC+bh8YZ/sFE9BUNTr8brrdsx8u6ZG+VEg/FoPREkhoP7U6r4DDleSGzfu8wI4ocL8= vagrant@ubuntu
  - l_elfe:
    username: l_elfe
    password: l_elfe
    homedir: /home/l_elfe
    access: 
    groups: admin,sudo
    ssh_public_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzMYPGClWlII1ZlRZUBf43ECExsr3Wd2fJzWtEgsCQZO+ztrmu7uAP+ovEndTDPJe1q3m9XntDSEI7jjQNM6h4enY1M7f7v1r/C7UHgLhqAP3PaEVcCq3yFcscnB4jOLLG8SonqVJO16L84J+Jwb2LwfsMyc0tUmtBqh4rbePzwrj9+ukExsRiR/7R4NqvBEYV/CFFej/cRRZeoaJJmWcGULJHXMvSK0jwFQOgpfXL6DpXQlLRwdl5xW465ZMXuOm4BixjWDkDO+8HVhOD4DxitixBmjUaGdccMfKH33Z031fdSYMqw8kBAfwoyU4CJzQWo39Maf1cThAuUFqza6/TMn7aWlpvcgX1DKVV6Qit788t7bf5DavotLb0ptA3K8W0dsR3TBg/Ixxtbt02I+RR7h7ghCPpbnNmJbblsGyvzJiDvw56srUoE8D67XGuBHC+bh8YZ/sFE9BUNTr8brrdsx8u6ZG+VEg/FoPREkhoP7U6r4DDleSGzfu8wI4ocL8= vagrant@ubuntu
  - le_ranger:
    username: le_ranger
    password: le_ranger
    homedir: /home/le_ranger
    access: 
    groups: admin,sudo
    ssh_public_key: ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCzMYPGClWlII1ZlRZUBf43ECExsr3Wd2fJzWtEgsCQZO+ztrmu7uAP+ovEndTDPJe1q3m9XntDSEI7jjQNM6h4enY1M7f7v1r/C7UHgLhqAP3PaEVcCq3yFcscnB4jOLLG8SonqVJO16L84J+Jwb2LwfsMyc0tUmtBqh4rbePzwrj9+ukExsRiR/7R4NqvBEYV/CFFej/cRRZeoaJJmWcGULJHXMvSK0jwFQOgpfXL6DpXQlLRwdl5xW465ZMXuOm4BixjWDkDO+8HVhOD4DxitixBmjUaGdccMfKH33Z031fdSYMqw8kBAfwoyU4CJzQWo39Maf1cThAuUFqza6/TMn7aWlpvcgX1DKVV6Qit788t7bf5DavotLb0ptA3K8W0dsR3TBg/Ixxtbt02I+RR7h7ghCPpbnNmJbblsGyvzJiDvw56srUoE8D67XGuBHC+bh8YZ/sFE9BUNTr8brrdsx8u6ZG+VEg/FoPREkhoP7U6r4DDleSGzfu8wI4ocL8= vagrant@ubuntu
EOF
ansible-vault encrypt users_password.yml
# password: users_password
ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml -e @users_password.yml --ask-vault-pass

# 2. Support de plusieurs OS
cat << EOF >> inventories/vagrant_lab/hosts.ini
node4.tp2.cloud
EOF
# update /etc/hosts, add the line "192.168.1.6 node4.tp2.cloud"
cat << EOF > inventories/vagrant_lab/group_vars/ynov.yml
common_groups_to_add:
  - admin
  - sudo

users:
  - le_nain:
    username: le_nain
    password: le_nain
    homedir: /home/le_nain
    access: 
    groups: admin,sudo
  - l_elfe:
    username: l_elfe
    password: l_elfe
    homedir: /home/l_elfe
    access: 
    groups: admin,sudo
  - le_ranger:
    username: le_ranger
    password: le_ranger
    homedir: /home/le_ranger
    access: 
    groups: admin,sudo

hosts_deploy_webapp:
  - node1.tp2.cloud:
    host_hostname: node1.tp2.cloud
    host_ip: 192.168.1.3
  - node2.tp2.cloud:
    host_hostname: node2.tp2.cloud
    host_ip: 192.168.1.4
hosts_deploy_webapp:
  - node1.tp2.cloud:
    host_hostname: node1.tp2.cloud
    host_ip: 192.168.1.3
    host_port: 8083
  - node2.tp2.cloud:
    host_hostname: node2.tp2.cloud
    host_ip: 192.168.1.4
    host_port: 8084
hosts_deploy_webapp:
  - node1.tp2.cloud:
    host_hostname: node1.tp2.cloud
    host_ip: 192.168.1.3
    host_port: 8083
EOF
cat << EOF > roles/common/tasks/users.yml
- name: Add common groups
  ansible.builtin.group:
    name: "{{ item }}"
    state: present
  with_items: "{{ common_groups_to_add }}"

- name: Add common users
  ansible.builtin.user:
    name: "{{ item.username }}"
    home: "{{ item.homedir }}"
    groups: "{{ item.groups }}"
  with_items: "{{ users }}"

- name: Set authorized key taken
  ansible.posix.authorized_key:
    user: "{{ item.username }}"
    state: present
    key: "{{ item.ssh_public_key }}"
  with_items: "{{ users }}"
EOF
cat << EOF > roles/nginx/tasks/install.yml
- name: Install NGINX
  ansible.builtin.package:
    name: nginx
    state: present
EOF
cat << EOF > roles/webapp/tasks/install.yml
- name: Install NGINX
  ansible.builtin.package:
    name: nginx
    state: present

- name: Allow all access to tcp port 80
  ansible.builtin.ufw:
    rule: allow
    port: "80"
  when: ansible_distribution == 'Debian' or ansible_distribution == 'Ubuntu'

- name: Allow all access to tcp port 80
  ansible.posix.firewalld:
    port: 80/tcp
    permanent: true
    state: enabled
  when: ansible_distribution == 'CentOS'
EOF
cat << EOF > inventories/vagrant_lab/group_vars/ynov.yml
common_groups_to_add:
  - admin
  - sudo

users:
  - le_nain:
    username: le_nain
    password: le_nain
    homedir: /home/le_nain
    access: 
    groups: admin,sudo
  - l_elfe:
    username: l_elfe
    password: l_elfe
    homedir: /home/l_elfe
    access: 
    groups: admin,sudo
  - le_ranger:
    username: le_ranger
    password: le_ranger
    homedir: /home/le_ranger
    access: 
    groups: admin,sudo

hosts_deploy_webapp:
  - node1.tp2.cloud:
    host_hostname: node1.tp2.cloud
    host_ip: 192.168.1.3
    host_port: 8081
  - node2.tp2.cloud:
    host_hostname: node2.tp2.cloud
    host_ip: 192.168.1.4
    host_port: 8082
  - node4.tp2.cloud:
    host_hostname: node4.tp2.cloud
    host_ip: 192.168.1.6
    host_port: 8084
EOF
cat << EOF > inventories/vagrant_lab/host_vars/node1.tp2.cloud.yml
vhosts_to_add:
  - test2:
    nginx_servername: test2
    nginx_port: 8082
    nginx_webroot: /var/www/html/test2
    nginx_index_content: "<h1>teeeeeest 2</h1>"
  - test3:
    nginx_servername: test3
    nginx_port: 8083
    nginx_webroot: /var/www/html/test3
    nginx_index_content: "<h1>teeeeeest 3</h1>"
EOF
cat << EOF > inventories/vagrant_lab/host_vars/node3.tp2.cloud.yml
is_rproxy: true
EOF
cat << EOF > inventories/vagrant_lab/host_vars/node4.tp2.cloud.yml
vhosts_to_add:
  - test4:
    nginx_servername: test4
    nginx_port: 8084
    nginx_webroot: /var/www/html/test4
    nginx_index_content: "<h1>teeeeeest 4</h1>"
  - test5:
    nginx_servername: test5
    nginx_port: 8085
    nginx_webroot: /var/www/html/test5
    nginx_index_content: "<h1>teeeeeest 5</h1>"
EOF
cat << EOF > roles/nginx/tasks/main.yml
- name: Install NGINX
  import_tasks: install.yml
  notify: "Restart nginx"

- name: Configure NGINX
  import_tasks: config.yml
  notify: "Restart nginx"

- name: Deploy VirtualHosts
  import_tasks: add_vhosts.yml
  when: vhosts_to_add is defined
  notify: "Restart nginx"

- name: Delete VirtualHosts
  import_tasks: remove_vhosts.yml
  when: vhosts_to_remove is defined
  notify: "Restart nginx"
EOF
cat << EOF > roles/nginx/tasks/add_vhosts.yml
- name: Create webroot
  file:
    path: "{{ item.nginx_webroot }}"
    state: directory
  with_items: "{{ vhosts_to_add }}"

- name: Create index
  copy:
    dest: "{{ item.nginx_webroot }}/index.html"
    content: "{{ item.nginx_index_content }}"
    mode: '0644'
  with_items: "{{ vhosts_to_add }}"

- name: NGINX Virtual Host
  template:
    src: vhost.conf.j2
    dest: /etc/nginx/conf.d/{{ item.nginx_servername }}.conf
    mode: '0644'
  with_items: "{{ vhosts_to_add }}"

- name: Allow all access to tcp port 80
  ansible.builtin.ufw:
    rule: allow
    port: "{{ item.nginx_port }}"
  with_items: "{{ vhosts_to_add }}"
  when: ansible_distribution == 'Debian' or ansible_distribution == 'Ubuntu'

- name: Allow all access to tcp port 80
  ansible.posix.firewalld:
    port: "{{ item.nginx_port }}/tcp"
    permanent: true
    state: enabled
  with_items: "{{ vhosts_to_add }}"
  when: ansible_distribution == 'CentOS'
EOF
rm roles/webapp/tasks/remove_vhosts.yml \
  roles/webapp/tasks/config.yml \
  roles/webapp/tasks/add_vhosts.yml
rm -r roles/webapp/defaults/ \
  roles/webapp/templates/ \
  roles/webapp/files/
cat << EOF > playbooks/main.yml
- hosts: ynov
  become: true #besoin pour executer le playbook en sudo
  roles:
    - common
    - nginx
    - role: webapp
      when: ((ansible_distribution == 'Debian' or ansible_distribution == 'Ubuntu') and ansible_facts.enp0s8.ipv4.address in ip_hosts_add_webapp) or
        ansible_distribution == 'CentOS' and ansible_facts.eth1.ipv4.address in ip_hosts_add_webapp
    - role: rproxy
      when: is_host_rproxy is defined and is_host_rproxy == true
EOF
cat << EOF > roles/webapp/tasks/main.yml
- name: Install NGINX
  import_tasks: install.yml
  notify: "Restart nginx"
EOF
cat << EOF > inventories/vagrant_lab/group_vars/ynov.yml
common_groups_to_add:
  - admin
  - sudo

users:
  - le_nain:
    username: le_nain
    password: le_nain
    homedir: /home/le_nain
    access: 
    groups: admin,sudo
  - l_elfe:
    username: l_elfe
    password: l_elfe
    homedir: /home/l_elfe
    access: 
    groups: admin,sudo
  - le_ranger:
    username: le_ranger
    password: le_ranger
    homedir: /home/le_ranger
    access: 
    groups: admin,sudo

ip_hosts_add_webapp:
  - "192.168.1.3"
  - "192.168.1.4"
  - "192.168.1.6"
EOF
mkdir roles/webapp/templates
cat << EOF > roles/webapp/templates/nginx_index_content.html.j2
welcome to the webapp of the host {{ inventory_hostname }}
EOF
cat << EOF >> roles/webapp/tasks/main.yml
- name: Configure NGINX
  import_tasks: config.yml
  notify: "Restart nginx"
EOF
cat << EOF > roles/webapp/tasks/config.yml
  - name: Insert Index Page
    template:
      src: nginx_index_content.html.j2
      dest: /var/www/html/index.nginx-debian.html
    when: ansible_distribution == 'Debian' or ansible_distribution == 'Ubuntu'

  - name: Insert Index Page
    template:
      src: nginx_index_content.html.j2
      dest: /usr/share/nginx/html/index.html
    when: ansible_distribution == 'CentOS'
EOF
curl webapp.com # Sur node3.tp2.cloud, exécuter 3 fois pour voir que nginx change d'IP à chanque fois
cat << EOF > roles/nginx/tasks/config.yml
- name : Main NGINX config file
  copy:
    src: debian_and_ubuntu_nginx.conf
    dest: /etc/nginx/nginx.conf

- name : Main NGINX config file
  copy:
    src: centos_nginx.conf
    dest: /etc/nginx/nginx.conf
EOF
mv roles/nginx/files/nginx.conf roles/nginx/files/debian_and_ubuntu_nginx.conf
cp roles/nginx/files/debian_and_ubuntu_nginx.conf roles/nginx/files/centos_nginx.conf
# enlever la ligne "user www-data;" de roles/nginx/files/centos_nginx.conf
```