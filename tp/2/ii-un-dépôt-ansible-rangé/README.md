```bash
# II. Un dépôt Ansible rangé
# 1. Structure du dépôt : inventaires
mkdir inventories/
mkdir inventories/vagrant_lab/
mv hosts.ini inventories/vagrant_lab/
ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/test.yml

# 2. Structure du dépôt : rôles
cat << EOF > ansible.cfg
[defaults]
roles_path = ./roles
EOF
mkdir roles/
mkdir roles/common/
mkdir roles/common/tasks/
cat << EOF > roles/common/tasks/main.yml
- name: Install common packages
  import_tasks: packages.yml
EOF
cat << EOF > roles/common/tasks/packages.yml
- name: Install common packages
  ansible.builtin.package:
    name: "{{ item }}"
    state: present
  with_items: "{{ common_packages }}" # ceci permet de boucler sur la liste common_packages
EOF
mkdir roles/common/defaults/
cat << EOF > roles/common/defaults/main.yml
common_packages:
  - vim
  - git
EOF
cat << EOF > playbooks/main.yml
- hosts: ynov
  become: true #besoin pour executer le playbook en sudo
  roles:
    - common
EOF
ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml

# 3. Structure du dépôt : variables d'inventaire
mkdir inventories/vagrant_lab/host_vars
cat << EOF > inventories/vagrant_lab/host_vars/node1.tp2.cloud.yml
#utilise var que pour machine node1.tp2.cloud, permet de ne pas utiliser var dans le fichier hosts.ini
common_packages:
  - vim
  - git
  - rsync
EOF
ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml
mkdir inventories/vagrant_lab/group_vars/
cat << EOF > inventories/vagrant_lab/group_vars/ynov.yml
users:
  - le_nain
  - l_elfe
  - le_ranger
EOF
cat << EOF >> roles/common/tasks/main.yml
- name: Add common users
  import_tasks: users.yml
EOF
cat << EOF > roles/common/tasks/users.yml
- name: Add common users
  ansible.builtin.user:
    name: "{{ item }}"
    state: present
  with_items: "{{ users }}" # ceci permet de boucler sur la liste users
EOF
ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml

# 4. Structure du dépôt : rôle avancé
mkdir roles/nginx/
mkdir roles/nginx/tasks
cat << EOF > roles/nginx/tasks/main.yml
- name: Install NGINX
  import_tasks: install.yml

- name: Configure NGINX
  import_tasks: config.yml

- name: Deploy VirtualHosts
  import_tasks: vhosts.yml
EOF
cat << EOF > roles/nginx/tasks/install.yml
- name: Install NGINX
  apt:
    name: nginx
    state: present
EOF
mkdir roles/nginx/files/
sudo apt install -y nginx
cp /etc/nginx/sites-enabled/default .
sudo apt remove -y nginx
mv default nginx.conf
mv nginx.conf roles/nginx/files/
mkdir roles/nginx/templates/
cat << EOF > roles/nginx/templates/vhost.conf.j2
server {
        listen {{ nginx_port }} ;
        server_name {{ nginx_servername }};

        location / {
            root {{ nginx_webroot }};
            index index.html;
        }
}
EOF
cat << EOF > roles/nginx/tasks/config.yml
- name : Main NGINX config file
  copy:
    src: nginx.conf # pas besoin de préciser de path, il sait qu'il doit chercher dans le dossier files/
    dest: /etc/nginx/nginx.conf
EOF
mkdir roles/nginx/defaults/
cat << EOF > roles/nginx/defaults/main.yml
nginx_servername: test
nginx_port: 8080
nginx_webroot: /var/www/html/test
nginx_index_content: "<h1>teeeeeest</h1>"
EOF
cat << EOF > roles/nginx/tasks/vhosts.yml
- name: Create webroot
  file:
    path: "{{ nginx_webroot }}"
    state: directory

- name: Create index
  copy:
    dest: "{{ nginx_webroot }}/index.html"
    content: "{{ nginx_index_content }}"

- name: NGINX Virtual Host
  template:
    src: vhost.conf.j2
    dest: /etc/nginx/conf.d/{{ nginx_servername }}.conf
EOF
cat << EOF > playbooks/main.yml
- hosts: ynov
  become: true #besoin pour executer le playbook en sudo
  roles:
    - common
    - nginx
EOF
ansible-playbook -i inventories/vagrant_lab/hosts.ini playbooks/main.yml
curl test -L # dans 192.168.1.4

# 5. Gérer la suppression
cat << EOF > roles/nginx/tasks/remove_vhosts.yml
- name: Delete webroot
  file:
    path: "{{ item.nginx_webroot }}"
    state: absent
  with_items: "{{ vhosts_to_remove }}"

- name: Delete NGINX Virtual Host
  file:
    path: "/etc/nginx/conf.d/{{ item.nginx_servername }}.conf"
    state: absent
  with_items: "{{ vhosts_to_remove }}"
EOF
```