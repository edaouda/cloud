```bash
# II. Images
# 🌞 Construire votre propre image
cat << EOF > Dockerfile
FROM alpine:3.15.4

RUN apk update && apk add apache2

RUN ["echo", "Voici la page d'accueil personnalisée du serveur Apache", ">", "/var/www/localhost/htdocs/index.html"]

CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]

EXPOSE 80
EOF
sudo docker build -t apache-app .
docker run -d \
    -p 8890:80 \
    --name container-apache-app apache-app
curl localhost:8890

cat << EOF > 

EOF
```
