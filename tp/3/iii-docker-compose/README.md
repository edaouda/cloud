```bash
# III. docker-compose
# 🌞 Créer un docker-compose.yml
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --help
<< EOF > 
Usage:
  docker-compose [-f <arg>...] [--profile <name>...] [options] [--] [COMMAND] [ARGS...]

Options:
  -f, --file FILE             Specify an alternate compose file
                              (default: docker-compose.yml)
  -p, --project-name NAME     Specify an alternate project name
                              (default: directory name)
  --profile NAME              Specify a profile to enable; ex: backend, lance les services avec le profile backend
  --verbose                   Show more output

Commands:
  build              Build or rebuild services
  config             Validate and view the Compose file
  create             Create services
  down               Stop and remove resources
  exec               Execute a command in a running container
  images             List images
  kill               Kill containers
  logs               View output from containers
  pause              Pause services
  port               Print the public port for a port binding
  ps                 List containers
  restart            Restart services
  rm                 Remove stopped containers
  run                Run a one-off command
  scale              Set number of containers for a service
  start              Start services
  stop               Stop services
  top                Display the running processes
  up                 Create and start containers
EOF

cat << EOF > docker-compose.yml
version: "3.8"

services:
  db:
    image: mysql:8.0.28
    restart: always
    container_name: container-db
    environment:
      - MYSQL_DATABASE= mydb
      - MYSQL_USER= myuser
      - MYSQL_PASSWORD= mypassword
      - MYSQL_ROOT_PASSWORD= mypassword

  web:
    image: nextcloud:23.0.3
    restart: always
    container_name: container-web
    ports:
      - 8891:80
    depends_on:
      - db
    links:
      - "db"
    environment:
      - MYSQL_DATABASE= mydb
      - MYSQL_USER= myuser
      - MYSQL_PASSWORD= mypassword
      - MYSQL_HOST= db
EOF
MYSQL_HOST & MYSQL_HOST ne sont pas une mê adresse IP ? (ex: 172.17.0.4)
curl localhost:8891

# Version 2
cat << EOF > Dockerfile
FROM apache-app

RUN apk add nextcloud-initscript

RUN apk add nginx php8-fpm

RUN rm /etc/nginx/http.d/default.conf

RUN apk add lighttpd php7-cgi

# RUN grep /etc/lighttpd/lighttpd.conf "include"

RUN sh /etc/lighttpd/ start

# RUN systemctl start lighttpd.service

# RUN ln -s /usr/share/webapps/nextcloud /var/www/localhost/htdocs

# CMD ["apache2-foreground"]
EOF
docker build -t nextcloud-image .
docker run -d \
    -p 8892:80 \
    --name nextcloud-app nextcloud-image
curl localhost:8890

cat << EOF > docker-compose.yml
version: "3.8"

services:
  db:
    image: mysql:8.0.28
    restart: always
    container_name: container-db
    environment:
      - MYSQL_DATABASE= mydb
      - MYSQL_USER= myuser
      - MYSQL_PASSWORD= mypassword
      - MYSQL_ROOT_PASSWORD= mypassword

  web:
    image: nextcloud-image
    container_name: nextcloud-app
    ports:
      - "8891:80"
    depends_on:
      - db
    links:
      - "db"
EOF
lighttpd -f /etc/lighttpd/lighttpd.conf
cat /var/log/lighttpd/error.log
2022-05-09 12:53:42: (server.c.1568) server started (lighttpd/1.4.64)
2022-05-09 12:53:42: (gw_backend.c.557) bind failed for: unix:/run/lighttpd/lighttpd-fastcgi-php-124.socket-0: No such file or directory
2022-05-09 12:53:42: (gw_backend.c.1700) [ERROR]: spawning gw failed.
2022-05-09 12:53:42: (server.c.1572) Configuration of plugins failed. Going down.

# Version 3
cat << EOF > Dockerfile
FROM apache-app

RUN apk add nextcloud
RUN rm /var/www/localhost/htdocs/*
# RUN cp -r /usr/share/webapps/nextcloud/* /var/www/localhost/htdocs/
RUN ln -s /usr/share/webapps/nextcloud/  /var/www/localhost/htdocs/
# COPY httpd.conf /etc/apache2/httpd.conf # affiche une erreur "you don't have permission to access this resource"

CMD ["/usr/sbin/httpd", "-D", "FOREGROUND"]

EXPOSE 80
EOF
docker build -t nextcloud-image .

cat << EOF > docker-compose.yml
version: "3.8"

services:
  db:
    image: mysql:8.0.28
    restart: always
    environment:
      - MYSQL_DATABASE= mydb
      - MYSQL_USER= myuser
      - MYSQL_PASSWORD= mypassword
      - MYSQL_ROOT_PASSWORD= mypassword

  web:
    image: nextcloud-image
    # container_name: nextcloud-app
    ports:
      - "8891:80"
    depends_on:
      - db
    links:
      - "db"
EOF
docker-compose up -d
curl localhost:8891

# Avec RUN cp -r /usr/share/webapps/nextcloud/* /var/www/localhost/htdocs/
<!DOCTYPE html>
<html>
<head>
        <script> window.location.href="index.php"; </script>
        <meta http-equiv="refresh" content="0; URL=index.php">
</head>
</html>

# Avec RUN ln -s /usr/share/webapps/nextcloud/  /var/www/localhost/htdocs/
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2 Final//EN">
<html>
 <head>
  <title>Index of /</title>
 </head>
 <body>
<h1>Index of /</h1>
<ul><li><a href="nextcloud/"> nextcloud/</a></li>
</ul>
<address>Apache/2.4.53 (Unix) Server at localhost Port 8891</address>
</body></html>

cat << EOF > 

EOF
```