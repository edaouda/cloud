```bash
# I. Docker
# 1. Install
vagrant ssh cloud-vm-tp3
sudo apt-get update
sudo apt-get install -y \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
sudo apt-get install -y docker-ce docker-ce-cli containerd.io
sudo systemctl start docker
sudo usermod -aG docker $(whoami)

logout
vagrant ssh cloud-vm-tp3
docker --version

# 2. Vérifier l'install
# Info sur l'install actuelle de Docker
docker info
  #Server Version: 20.10.13=version de docker
  #Cgroup Driver: cgroupfs, =cgroup ?
# Liste des conteneurs actifs
docker ps
# Liste de tous les conteneurs
docker ps -a, -all
# Liste des images disponibles localement
docker images
# Lancer un conteneur debian
docker run debian #telecharge image sur docker hub
docker run -d debian sleep 99999
docker run -it debian bash #exit le conteneur lorsqu'on exit
# Consulter les logs d'un conteneur
docker ps # on repère l'ID/le nom du conteneur voulu
  #CONTAINER ID   IMAGE     COMMAND         CREATED         STATUS         PORTS     NAMES
  #ffa66d7230dd   debian    "sleep 99999"   2 minutes ago   Up 2 minutes             practical_wing
docker logs ffa66d7230dd # avoir l'historique des commandes
docker logs -f ffa66d7230dd # suit l'arrivée des logs en temps réel
# Exécuter un processus dans un conteneur actif
docker ps # on repère l'ID/le nom du conteneur voulu
docker exec ffa66d7230dd cd ls
docker exec ffa66d7230dd ls
docker exec -it ffa66d7230dd bash # permet de récupérer un shell bash dans le conteneur ciblé
docker --help
<< EOF > 
   config      Manage Docker configs
   container   Manage containers
   context     Manage contexts
   image       Manage images
   service     Manage services
   stack       Manage Docker stacks
   system      Manage Docker
   trust       Manage trust on Docker images, docker trust sign debian:latest, failed to sign docker.io/library/debian:latest: you are not authorized to perform this operation: server returned 401 <- I don't have authorization on the registry
   build       Build an image from a Dockerfile
   commit      Create a new image from a container's changes
   cp          Copy files/folders between a container and the local filesystem
   login       Log in to a Docker registry, peut utiliser autre image registry mais fait du commercial, standard registry https://github.com/distribution/distribution
   rename Rename a container docker rename ffa66d7230dd new-name-container
   rm Remove one or more containers
   restart Restart one or more containers, change pas l'état du conteneur ?
   search Search the Docker Hub for images, ~recherche https://hub.docker.com/search?q=debian&type=image
   stats       Display a live stream of container(s) resource usage statistics
   top         Display the running processes of a container
   conclusion: du help, pas sur si certains commandes sont fait pour image ou container (ex: rm)
EOF
docker run --help
<< EOF > 
      --add-host list                  Add a custom host-to-IP mapping (host:ip); ex: --add-host=docker:93.184.216.34 ajoute dans /etc/hosts ; 1 conteneur peut avoir une IP ou pas: oui
      --cap-add list                   Add Linux capabilities=droit user (cf. https://man7.org/linux/man-pages/man7/capabilities.7.html)
      --cap-drop list                  Drop Linux capabilities
      --cpus decimal                   Number of CPUs
  -d, --detach                         Run container in background and print container ID, n'affiche pas print ~ command &
      --device list                    Add a host device to the container ~ volume mount
      --disable-content-trust          Skip image verification (default true)
      --dns list                       Set custom DNS servers; ex: 8.8.8.8
      --entrypoint string              Overwrite the default ENTRYPOINT of the image
  -e, --env list                       Set environment variables
      --env-file list                  Read in a file of environment variables
      --expose list                    Expose a port or a range of ports
      --group-add list                 Add additional groups to join; ex: audio
      --health-cmd string              lance une commande qui vérifie la santé du conteneur, ex: "curl --fail http://localhost:8091/pools || exit 1"
  -h, --hostname string                Container host name
      --ip string                      IPv4 address (e.g., 172.30.100.104)
  -l, --label list                     Set meta data on a container
  -m, --memory bytes                   Memory limit
      --mount mount                    Attach a filesystem mount to the container
      --name string                    Assign a name to the container
      --network network                Connect a container to a network; ex: --network=my-net
      --oom-score-adj int              Tune host's OOM preferences (-1000 to 1000)
      --pid string                     PID namespace to use
      --pids-limit int                 Tune container pids limit (set -1 for unlimited)
  -p, --publish list                   Publish a container's port(s) to the host
  -P, --publish-all                    Publish all exposed ports to random ports
      --pull string                    Pull image before running ("always"|"missing"|"never") (default "missing")
      --restart string                 Restart policy to apply when a container exits (default "no"); ex: always redémarre si meurt
      --rm                             écrase le conteneur si exist, pas compris (cf. https://docs.docker.com/engine/reference/run/#clean-up---rm)
      --runtime string                 Runtime to use for this container
  -v, --volume list                    Bind mount a volume
      --volumes-from list              Mount volumes from the specified container(s)
  -w, --workdir string                 Working directory inside the container
EOF

# 3. L'installation de Docker
# 🌞 Déterminez...
# le path du dossier de données de Docker: docker info | grep "Docker Root Dir" -> /var/lib/docker
# pourquoi est-ce qu'être membre du groupe docker permet de l'utiliser ?: Docker lancant les conteneurs en root, les membres du groupe docker sont root pour permettre d'utiliser Docker sans avoir besoin d'être root
# le path du fichier de conf de Docker: /etc/docker/daemon.json
# 🌞 Editer le fichier de configuration du Démon Docker
mkdir /home/vagrant/docker-data-root
sudo vi /etc/docker/daemon.json
<< content-/etc/docker/daemon.json > 
{
    "data-root": "/home/vagrant/docker-data-root",
    "oom-score-adjust": -400
}
content-/etc/docker/daemon.json
sudo systemctl restart docker.service
docker info | grep "Docker Root Dir" # Docker Root Dir: /home/vagrant/docker-data-root
sudo ls docker-data-root/ # buildkit  containers  image  network  overlay2  plugins  runtimes  swarm  tmp  trust  volumes
# 🌞 Analyser les processus liés au démon
ps aux # u: user, x: ~auxiliaire, list the processes without a controlling terminal, a: all
ps aux | grep docker
# 🌞 Analyse les processus liés à chaque conteneur
docker run --name analyser-processus-conteneur -d debian sleep 99999 # avec un nom, nom est affiché dans les processus ?
ps aux | grep docker # toujours 2 processus, normal ?
# 4. Lancement de conteneurs
docker stop (arrete le conteneur) != docker rm (Error response from daemon: You cannot remove a running container 66d8b1c8b8d3aa3b2a0bd30cd31ed4a764505bfa54be70bf462cfeedc65f15af. Stop the container before attempting removal or force remove)
docker run --name web -d -v /html:/usr/share/nginx/html -p 8888:80 nginx
curl localhost:8888 # 403 Forbidden
docker exec web curl localhost # 403 Forbidden
# 🌞 Utiliser la commande docker run
docker exec web cat /etc/nginx/nginx.conf > nginx.conf
echo "<h1>Voici la page de l'app NGINX personnalisé</h1>" > ./html/index.html
docker run -v /nginx.conf:/etc/nginx/nginx.conf \
    -v /html:/usr/share/nginx/html \
    -p 8889:80 \
    --memory 1GB \
    --cpus 1 \
    --name app-nginx \
    --user vagrant nginx # bof

docker run -v /home/vagrant/nginx.conf:/etc/nginx/nginx.conf \
    -v /html:/usr/share/nginx/html \
    -p 8889:80 \
    --memory 1GB \
    --cpus 1 \
    --name app-nginx \
    --user 1000 nginx # ça va mieux mais pas toujours



cat << EOF > 

EOF
```